export interface EngineOption {
    adType: 'facebook' | 'google' | 'naver';
    adContent: string;
}
