import { Controller, Get, Query } from '@nestjs/common';
import { AppService } from './app.service';

@Controller('ad')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  sendQueue(@Query('content') adCotent: string, @Query('type') adType: string) {
    return this.appService.sendQueue(adType, adCotent);
  }
}
