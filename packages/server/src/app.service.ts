import { Injectable } from '@nestjs/common';
import { SendMessageCommand } from '@aws-sdk/client-sqs';
import AWS from 'aws/config';
import { sqsClient } from 'aws/sqs';

@Injectable()
export class AppService {
  async sendQueue(adType: string, adContent: string) {
    const params = {
      DelaySeconds: 0,
      MessageGroupId: `${Math.random() * 1000}`,
      MessageDeduplicationId: `${Math.random() * 1000}`,
      MessageAttributes: {
        AdType: {
          DataType: 'String',
          StringValue: adType,
        },
        AdContent: {
          DataType: 'String',
          StringValue: adContent,
        },
      },
      MessageBody: JSON.stringify({
        adType,
        adContent,
      }),
      QueueUrl:
        'https://sqs.ap-northeast-2.amazonaws.com/835771297188/myQueueTest.fifo', //SQS_QUEUE_URL; e.g., 'https://sqs.REGION.amazonaws.com/ACCOUNT-ID/QUEUE-NAME'
    };

    const run = async () => {
      try {
        const data = await sqsClient.send(new SendMessageCommand(params));
        console.log(`
            -- 응답성공
            광고타입 : ${adType},
            광고내용 : ${adContent}`);
        return data;
      } catch (err) {
        console.log('Error', err);
      }
    };
    run();

    return `-- 응답성공
            광고타입 : ${adType},
            광고내용 : ${adContent}`;
  }
}
