import { SQSClient } from '@aws-sdk/client-sqs';
const REGION = process.env.REGION;

const sqsClient = new SQSClient({
  credentials: {
    accessKeyId: process.env.ACCESS_KEY_ID || '',
    secretAccessKey: process.env.SECRET_ACCESS_KEY || '',
  },
  region: REGION,
});

export { sqsClient };
