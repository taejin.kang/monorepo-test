import { adService } from '../service/ad.service';
import sqsService from '../service/sqs.service';

const service = {
    ad: new adService(),
    sqs: sqsService
}

export default service