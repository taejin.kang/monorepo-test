import {
  ReceiveMessageCommand,
  DeleteMessageCommand,
} from  "@aws-sdk/client-sqs";
import { sqsClient } from '../../aws/sqs';

const queueURL = 'https://sqs.ap-northeast-2.amazonaws.com/835771297188/myQueueTest.fifo';
const params = {
  AttributeNames: ["SentTimestamp"],
  MaxNumberOfMessages: 10,
  MessageAttributeNames: ["All"],
  QueueUrl: queueURL,
  VisibilityTimeout: 20,
  WaitTimeSeconds: 0,
};

const getMessage = async () => {
  const data = await sqsClient.send(new ReceiveMessageCommand(params));
  if (data.Messages !== undefined) {
    await sqsClient.send(new DeleteMessageCommand({
        QueueUrl: queueURL,
        ReceiptHandle: data.Messages[0].ReceiptHandle,
    }));
    const message = JSON.parse(data.Messages[0].Body || '')
    return {
      status: true,
      message: {
        adType: message.adType,
        adContent: message.adContent
      }
    }
  } else {
    return {
      status: false,
      message: {
        adType: '',
        adContent: ''
      }
    }
  }
}

const sqsService = {
  getMessage,
}


export default sqsService