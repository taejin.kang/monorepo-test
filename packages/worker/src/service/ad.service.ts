import { adEngine } from '../../../engines';
import { EngineOption } from '../../../interface/engine.option.type';

export class adService {
    async make(engineOption: EngineOption) {
        const engine = await adEngine(engineOption);
        return await engine.makeAd(engineOption);
    }
}
