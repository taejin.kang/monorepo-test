import cron from 'node-cron';
import service from '../service'

let count = 0;

const adQueueSubscribe = cron.schedule('*/5 * * * * *', async () => {
    try {
        count+=1
        console.log(count)
        const sqsMessage = await service.sqs.getMessage();
        if (sqsMessage.status) await service.ad.make(
            sqsMessage.message
        );
        return;
    } catch (err) {
        console.log(err);
    }
})

export default adQueueSubscribe;