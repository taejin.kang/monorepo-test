import { EngineBase } from '../../bases/EngineBase';
import { EngineOption } from '../../../interface/engine.option.type';

export class Engine extends EngineBase {
    constructor(engineOption: EngineOption) {
        super(engineOption, 'google');
    }
}
