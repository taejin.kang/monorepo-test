import { EngineOption } from '../../interface/engine.option.type';
import { EngineOutput } from '../../interface/engine.output.type'

export abstract class EngineBase {
    engineOption: EngineOption;
    engineType: string;

    constructor(engineOption: EngineOption, engineType: string) {
        this.engineOption = engineOption;
        this.engineType = engineType;
    }

    async makeAd(engineOption: EngineOption): Promise<EngineOutput> {
        try {
            console.log('==================')
            console.log('엔진에 들어온 값', engineOption)
            console.log(`${this.engineType} 엔진 발생!`);
            return 'success';
        } catch (err) {
            console.log(err);
            return 'fail';
            //에러처리
        }
    }
}
