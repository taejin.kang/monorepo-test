import { EngineOption } from '../interface/engine.option.type';
import { EngineBase } from './bases/EngineBase';

export async function adEngine(engineOption: EngineOption): Promise<EngineBase> {
    const { Engine } = await import(`./engineType/${engineOption.adType}/Engine`);
    return new Engine(engineOption);
    
}
